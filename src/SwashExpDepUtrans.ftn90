subroutine SwashExpDepUtrans ( rp, rpo, u1, u0, fluxt )
!
!   --|-----------------------------------------------------------|--
!     | Delft University of Technology                            |
!     | Faculty of Civil Engineering and Geosciences              |
!     | Environmental Fluid Mechanics Section                     |
!     | P.O. Box 5048, 2600 GA  Delft, The Netherlands            |
!     |                                                           |
!     | Programmers: The SWASH team                               |
!   --|-----------------------------------------------------------|--
!
!
!     SWASH (Simulating WAves till SHore); a non-hydrostatic wave-flow model
!     Copyright (C) 2010-2023  Delft University of Technology
!
!     This program is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     This program is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with this program. If not, see <http://www.gnu.org/licenses/>.
!
!
!   Authors
!
!    1.00: Marcel Zijlema
!
!   Updates
!
!    1.00, January 2023: New subroutine
!
!   Purpose
!
!   Performs the time integration for the depth-averaged transport equations on triangular mesh
!
!   Method
!
!   The time integration is fully explicit.
!
!   The space discretization is a cell-centered finite volume discretization and
!   is consistent with the discretization of the global continuity equation.
!
!   The advective term is approximated by either first order upwind or higher order (flux-limited) scheme
!   (CDS, Fromm, BDF, QUICK, MUSCL, Koren, etc.). For the r-ratio the most upwave vertex of upwind cell is used.
!
!   The Thatcher-Harleman boundary condition is imposed at sea side for unsteady salt intrusion.
!   The constituent return time is given by the user.
!
!   M.L. Thatcher and R.F. Harleman
!   A mathematical model for the prediction of unsteady salinity intrusion in estuaries
!   Technical report 144, MIT, Massachusetts, USA, 1972
!
!   Modules used
!
    use ocpcomm4
    use SwashCommdata3
    use SwashTimecomm
    use SwashFlowdata, fluxttmp => fluxt, &
                       rptmp    => rp   , &
                       rpotmp   => rpo  , &
                       u0tmp    => u0   , &
                       u1tmp    => u1
    use SwanGriddata
    use SwanGridobjects
    use SwanCompdata
!
    implicit none
!
!   Argument variables
!
    real, dimension(nfaces)       , intent(out)   :: fluxt    ! total flux at faces
    real, dimension(ncells,ltrans), intent(inout) :: rp       ! concentration at current time level
    real, dimension(ncells)       , intent(out)   :: rpo      ! concentration at previous time level
    real, dimension(nfaces)       , intent(in)    :: u0       ! face velocity at previous time level
    real, dimension(nfaces)       , intent(in)    :: u1       ! face velocity at current time level
!
!   Local variables
!
    integer                                       :: btype    ! boundary type (see SwashUpdateUData.f90)
    integer                                       :: icell    ! cell index / loop counter over cells
    integer                                       :: icelll   ! left cell of present face
    integer                                       :: icellr   ! right cell of present face
    integer                                       :: icistb   ! counter for number of instable points
    integer, save                                 :: ient = 0 ! number of entries in this subroutine
    integer                                       :: iface    ! face index / loop counter over faces
    integer                                       :: j        ! loop counter
    integer                                       :: jc       ! loop counter
    integer                                       :: jf       ! loop counter
    integer                                       :: l        ! loop counter over constituents
    integer, dimension(3)                         :: v        ! vertices of present cell
    integer                                       :: vf1      ! first vertex of present face
    integer                                       :: vf2      ! second vertex of present face
    integer                                       :: vu       ! upwind vertex
    !
    real                                          :: area     ! area of present cell
    real                                          :: areal    ! area of left cell of present face
    real                                          :: arear    ! area of right cell of present face
    real                                          :: cfl      ! CFL number
    real                                          :: contrib  ! total contribution of transport flux per cell
    real                                          :: dif2d    ! horizontal eddy diffusivity coefficient in velocity point
    real                                          :: fac      ! a factor
    real                                          :: finp     ! interpolation factor
    real                                          :: fluxlim  ! flux limiter
    real                                          :: grad1    ! solution gradient
    real                                          :: grad2    ! another solution gradient
    real                                          :: lf       ! length of present face
    real                                          :: mass     ! total mass
    real                                          :: psm      ! Prandtl-Schmidt number
    real                                          :: qf       ! mass flux
    real                                          :: rdx      ! reciprocal of distance between circumcenters adjacent to face
    real                                          :: rproc    ! auxiliary variable with percentage of instable points
    real                                          :: rpu      ! averaged concentration in upwind vertex
    real                                          :: rsgn     ! sign for indicating face orientation
    real                                          :: stabmx   ! auxiliary variable with maximum diffusivity based stability criterion
    real                                          :: sumqf    ! sum of outgoing mass fluxes per cell
    real                                          :: u        ! local flow velocity
    real                                          :: theta    ! implicitness factor for flow velocity
    real                                          :: totarea  ! total area of all cells around vertex
    !
    character(120)                                :: msgstr   ! string to pass message
    !
    type(verttype), dimension(:), pointer         :: vert     ! datastructure for vertices with their attributes
    type(celltype), dimension(:), pointer         :: cell     ! datastructure for cells with their attributes
    type(facetype), dimension(:), pointer         :: face     ! datastructure for faces with their attributes
!
!   Structure
!
!   Description of the pseudo code
!
!   Source text
!
    if (ltrace) call strace (ient,'SwashExpDepUtrans')
    !
    ! point to vertex, cell and face objects
    !
    vert => gridobject%vert_grid
    cell => gridobject%cell_grid
    face => gridobject%face_grid
    !
    ! determine implicitness factor for flow velocity
    ! note: consistent with continuity equation
    !
    if ( mtimei == 1 ) then
       theta = 1.
    else if ( mtimei == 2 ) then
       theta = pnums(1)
    endif
    !
    ! if momentum equation has been skipped or Newton method is applied, check CFL criterion first
    !
    if ( (momskip .or. inewt /= 0) .and. ITEST >= 30 ) then
       !
       cflmax = -999.
       !
       do icell = 1, ncells
          !
          ! compute the sum of mass fluxes leaving the cell
          !
          sumqf = 0.
          !
          do jf = 1, cell(icell)%nof
             !
             ! face identifier
             !
             iface = cell(icell)%face(jf)%atti(FACEID)
             !
             ! get length of current face
             !
             lf = face(iface)%attr(FACELEN)
             !
             ! consider left and right cells of current face
             !
             icelll = face(iface)%atti(FACECL)
             icellr = face(iface)%atti(FACECR)
             !
             ! take into account orientation of the current face
             !
             if ( icell == icelll ) then
                rsgn =  1.
             else if ( icell == icellr ) then
                rsgn = -1.
             endif
             !
             ! compute mass flux at current face
             !
             u  = theta*u1(iface) + (1.-theta)*u0(iface)
             qf = rsgn * lf * hu(iface) * u
             !
             if ( qf > 0. ) sumqf = sumqf + qf
             !
          enddo
          !
          ! compute the "flow" Courant number
          !
          if ( hs(icell) > epsdry ) then
             !
             area = cell(icell)%attr(CELLAREA)
             !
             cfl = sumqf * dt / hs(icell) / area
             if ( cfl > cflmax ) cflmax = cfl
             !
          endif
          !
       enddo
       !
       ! give warning in case of CFL > 0.5
       !
       if ( cflmax > 0.5 ) then
          !
          call msgerr ( 1, 'The max-min property cannot be complied with!')
          call msgerr ( 0, 'If desired, reduce the time step' )
          !
       endif
       !
    endif
    !
    ! loop over constituents
    !
    do l = 1, ltrans
       !
       ! determine Prandtl-Schmidt number
       !
       if ( l == lsal .or. l == ltemp ) then
          psm = 0.7
       else if ( l == lsed ) then
          psm = 1.0
       endif
       !
       icistb = 0
       !
       ! store old concentrations
       !
       rpo(:) = rp(:,l)
       !
       ! compute advective flux and concentration at open boundaries using boundary conditions
       !
       do jf = 1, nfacesb
          !
          iface = jbface(jf)
          icell = face(iface)%atti(FACEC1)
          !
          ! get length of boundary face
          !
          lf = face(iface)%attr(FACELEN)
          !
          ! get area of boundary cell
          !
          area = cell(icell)%attr(CELLAREA)
          !
          btype = face(iface)%atti(FBTYPE)
          !
          if ( btype /= 1 ) then
             !
             ! --- boundary face open
             !
             ! consider left and right cells of current face
             !
             icelll = face(iface)%atti(FACECL)
             icellr = face(iface)%atti(FACECR)
             !
             ! take into account orientation of the current face
             !
             if ( icell == icelll ) then
                ! leaving the cell
                rsgn =  1.
             else if ( icell == icellr ) then
                ! entering the cell
                rsgn = -1.
             endif
             !
             ! compute mass flux at boundary face
             !
             u  = theta*u1(iface) + (1.-theta)*u0(iface)
             qf = lf * hu(iface) * u
             !
             if ( rsgn * qf > 0. ) then
                !
                ! outflow
                !
                fluxt(iface) = qf * rpo(icell)
                !
                bcrp(jf,1,l) = bcrpo(jf,1,l) - dt * rsgn * lf * u * ( bcrpo(jf,1,l) - rpo(icell) ) / area
                !
                if ( l == lsal ) then
                   coutu (jf,1) = bcrp(jf,1,l)
                   icretu(jf,1) = tcret
                endif
                !
             else
                !
                ! inflow
                !
                fluxt(iface) = qf * bcrpo(jf,1,l)
                !
                if ( l == lsal ) then
                   fac = max(icretu(jf,1),0.) / max(tcret,dt)
                   bcrp(jf,1,l) = coutu(jf,1) + 0.5 * ( cbndu(jf,1,l) - coutu(jf,1) ) * ( 1. + cos(fac*pi) )
                   if ( .not. icretu(jf,1) < 0. ) icretu(jf,1) = icretu(jf,1) - dt
                else
                   bcrp(jf,1,l) = cbndu(jf,1,l)
                endif
                !
             endif
             !
          else
             !
             ! --- boundary face closed: no advective flux
             !
             fluxt(iface) = 0.
             !
          endif
          !
       enddo
       !
       ! compute advective and diffusive fluxes at internal faces
       !
       do iface = 1, nfaces
          !
          if ( face(iface)%atti(FMARKER) == 0 .and. wetu(iface) == 1 ) then   ! internal wet face
             !
             finp = face(iface)%attr(FACELINPF)
             !
             ! get vertices of current face
             !
             vf1 = face(iface)%atti(FACEV1)
             vf2 = face(iface)%atti(FACEV2)
             !
             ! get length of current face
             !
             lf = face(iface)%attr(FACELEN)
             !
             ! get reciprocal distance between the centers adjacent to current face
             !
             rdx = face(iface)%attr(FACEDISTC)
             !
             ! consider left and right cells of current face
             !
             icelll = face(iface)%atti(FACECL)
             icellr = face(iface)%atti(FACECR)
             !
             areal = cell(icelll)%attr(CELLAREA)
             arear = cell(icellr)%attr(CELLAREA)
             !
             propsc = nint(pnums(46))
             kappa  = pnums(47)
             mbound = pnums(48)
             phieby = pnums(49)
             !
             ! compute mass flux at current face
             !
             u  = theta*u1(iface) + (1.-theta)*u0(iface)
             qf = lf * hu(iface) * u
             !
             ! compute the advective flux based on first order upwind
             !
             if ( qf > 0. ) then
                !
                fluxt(iface) = qf * rpo(icelll)
                !
             else
                !
                fluxt(iface) = qf * rpo(icellr)
                !
             endif
             !
             ! add second order approximation
             !
             if ( propsc /= 1 ) then
                !
                if ( qf > 0. ) then
                   !
                   ! get vertices of upwind cell
                   !
                   v(1) = cell(icelll)%atti(CELLV1)
                   v(2) = cell(icelll)%atti(CELLV2)
                   v(3) = cell(icelll)%atti(CELLV3)
                   !
                   ! search for most upwave vertex
                   !
                   do j = 1, 3
                      if ( v(j) /= vf1 .and. v(j) /= vf2 ) then
                         vu = v(j)
                         exit
                      endif
                   enddo
                   !
                   ! compute area-weighted averaged concentration at upwave vertex
                   !
                   rpu     = 0.
                   totarea = 0.
                   !
                   do jc = 1, vert(vu)%noc
                      !
                      icell = vert(vu)%cell(jc)%atti(CELLID)
                      !
                      area = cell(icell)%attr(CELLAREA)
                      !
                      rpu = rpu + area * rpo(icell)
                      !
                      totarea = totarea + area
                      !
                   enddo
                   !
                   rpu = rpu / totarea
                   !
                   ! compute solution gradients
                   !
                   grad1 = rpo(icellr) - rpo(icelll)
                   grad2 = rpo(icelll) - rpu
                   !
                   ! update flux
                   !
                   fluxt(iface) = fluxt(iface) + 0.5 * qf * fluxlim(grad1,grad2)
                   !
                else
                   !
                   ! get vertices of upwind cell
                   !
                   v(1) = cell(icellr)%atti(CELLV1)
                   v(2) = cell(icellr)%atti(CELLV2)
                   v(3) = cell(icellr)%atti(CELLV3)
                   !
                   ! search for most upwave vertex
                   !
                   do j = 1, 3
                      if ( v(j) /= vf1 .and. v(j) /= vf2 ) then
                         vu = v(j)
                         exit
                      endif
                   enddo
                   !
                   ! compute area-weighted averaged concentration at upwave vertex
                   !
                   rpu     = 0.
                   totarea = 0.
                   !
                   do jc = 1, vert(vu)%noc
                      !
                      icell = vert(vu)%cell(jc)%atti(CELLID)
                      !
                      area = cell(icell)%attr(CELLAREA)
                      !
                      rpu = rpu + area * rpo(icell)
                      !
                      totarea = totarea + area
                      !
                   enddo
                   !
                   rpu = rpu / totarea
                   !
                   ! compute solution gradients
                   !
                   grad1 = rpo(icelll) - rpo(icellr)
                   grad2 = rpo(icellr) - rpu
                   !
                   ! update flux
                   !
                   fluxt(iface) = fluxt(iface) + 0.5 * qf * fluxlim(grad1,grad2)
                   !
                endif
                !
             endif
             !
             ! compute effective horizontal diffusivity coefficient at current face
             !
             if ( hdiff > 0. ) then
                !
                dif2d = hdiff
                !
             else
                !
                if ( ihvisc == 2 .or. ihvisc == 3 ) then
                   !
                   dif2d = vnu2d(iface) / psm
                   !
                else
                   !
                   dif2d = 0.
                   !
                endif
                !
             endif
             !
             stabmx = ( finp*areal + (1.-finp)*arear ) / ( 3. * rdx * lf * dt )
             !
             ! check stability
             !
             if ( .not. dif2d < stabmx ) then
                dif2d  = stabmx
                icistb = icistb + 1
             endif
             !
             ! compute the diffusive flux and update total flux
             !
             fluxt(iface) = fluxt(iface) - dif2d * hu(iface) * lf * rdx * ( rpo(icellr) - rpo(icelll) )
             !
          else if ( face(iface)%atti(FMARKER) == 0 ) then
             !
             fluxt(iface) = 0.
             !
          endif
          !
       enddo
       !
       !  compute concentration in cells (based on finite volume approach)
       !
       do icell = 1, ncells
          !
          contrib = 0.
          !
          ! loop over faces of the cell
          !
          do jf = 1, cell(icell)%nof
             !
             ! face identifier
             !
             iface = cell(icell)%face(jf)%atti(FACEID)
             !
             ! consider left and right cells of current face
             !
             icelll = face(iface)%atti(FACECL)
             icellr = face(iface)%atti(FACECR)
             !
             ! take into account orientation of the current face
             !
             if ( icell == icelll ) then
                rsgn =  1.
             else if ( icell == icellr ) then
                rsgn = -1.
             endif
             !
             ! get advective/diffusive flux at current face and add to other faces of the cell
             !
             contrib = contrib + rsgn * fluxt(iface)
             !
          enddo
          !
          ! update in wet cell
          !
          if ( hs(icell) > epsdry ) then
             !
             area = cell(icell)%attr(CELLAREA)
             !
             rp(icell,l) = hso(icell) * rpo(icell) - dt * contrib / area
             !
             ! compute depth-averaged concentration
             !
             rp(icell,l) = rp(icell,l) / hs(icell)
             !
          endif
          !
       enddo
       !
       ! calculate total mass
       !
       if ( ITEST >= 30 ) then
          !
          mass = 0.
          !
          do icell = 1, ncells
             !
             ! area of cell
             !
             area = cell(icell)%attr(CELLAREA)
             !
             mass = mass + hs(icell) * area * rp(icell,l)
             !
          enddo
          !
          if ( l == lsal  ) write(PRINTF,101) mass
          if ( l == ltemp ) write(PRINTF,102) mass
          if ( l == lsed  ) write(PRINTF,103) mass*rhos
          !
       endif
       !
       ! give warning for instable points
       !
       if ( icistb > 0 ) then
          !
          rproc = 100.*real(icistb)/real(nfaces - nfacesb)
          !
          if ( .not. rproc < 1. ) then
             if ( l == lsal  ) write (msgstr,'(a,f5.1)') 'percentage of instable points for computing horizontal eddy diffusivity of salinity transport = ',rproc
             if ( l == ltemp ) write (msgstr,'(a,f5.1)') 'percentage of instable points for computing horizontal eddy diffusivity of heat transport = ',rproc
             if ( l == lsed  ) write (msgstr,'(a,f5.1)') 'percentage of instable points for computing horizontal eddy diffusivity of sediment transport = ',rproc
             call msgerr (1, trim(msgstr) )
          endif
          !
          if ( rproc > 30. ) then
             call msgerr ( 4, 'INSTABLE: unable to solve the transport equation!' )
             call msgerr ( 0, '          Please reduce the horizontal diffusivity coefficient!' )
             return
          endif
          !
       endif
       !
    enddo
    !
 101 format (2x,'the total mass associated with saline water is ',e14.8e2)
 102 format (2x,'the total mass associated with heat is ',e14.8e2)
 103 format (2x,'the total mass associated with suspended sediment ',e14.8e2)
    !
end subroutine SwashExpDepUtrans

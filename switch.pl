$tim = "FALSE";
$mpi = "FALSE";
$f95 = "FALSE";
$dos = "FALSE";
$unx = "FALSE";
$cry = "FALSE";
$sgi = "FALSE";
$imp = "FALSE";
$cvi = "FALSE";
$mv4 = "FALSE";
while ($ARGV[0] && $ARGV[0] =~ /^-.*/) {
    if    ($ARGV[0] =~ /-timg/)  { $tim = "TRUE"; }
    elsif ($ARGV[0] =~ /-mpi/)   { $mpi = "TRUE"; }
    elsif ($ARGV[0] =~ /-f95/)   { $f95 = "TRUE"; }
    elsif ($ARGV[0] =~ /-dos/)   { $dos = "TRUE"; }
    elsif ($ARGV[0] =~ /-unix/)  { $unx = "TRUE"; }
    elsif ($ARGV[0] =~ /-cray/)  { $cry = "TRUE"; }
    elsif ($ARGV[0] =~ /-sgi/)   { $sgi = "TRUE"; }
    elsif ($ARGV[0] =~ /-impi/)  { $imp = "TRUE"; }
    elsif ($ARGV[0] =~ /-cvis/)  { $cvi = "TRUE"; }
    elsif ($ARGV[0] =~ /-matl4/) { $mv4 = "TRUE"; }
    shift; # Remove the processed or unrecognized argument
}

# --- make a list of all files
@files = ();
foreach (@ARGV) {
   @files = (@files , glob );
}

# --- change each file if necessary
foreach $file (@files)
{
# --- set output file name
  if ($unx=~/TRUE/)
  {
    ($tempf)=split(/.ftn/, $file);
    $ext = ($file =~ m/ftn90/) ? "f90" : "f";
    $outfile = join(".",$tempf,$ext);
  }
  else
  {
    ($tempf)=split(/.ftn/, $file);
    $ext = ($file =~ m/ftn90/) ? "f90" : "for";
    $outfile = join(".",$tempf,$ext);
  }
# --- process file
  if (   (! -e $outfile)            #outfile doesn't exist
      || (-M $file < -M $outfile) ) #.ftn file recently modified
  {
    open file or die "can't open $file\n";
    open(OUTFILE,">".$outfile);
    while ($line=<file>)
    {
      $newline=$line;
      if ($tim=~/TRUE/) {$newline=~s/^!TIMG//;}
      if ($mpi=~/TRUE/) {$newline=~s/^!MPI//;}
      if ($f95=~/TRUE/) {$newline=~s/^!F95//;}
      if ($dos=~/TRUE/) {$newline=~s/^!DOS//;}
      if ($unx=~/TRUE/) {$newline=~s/^!UNIX//;}
      if ($cry=~/TRUE/) {$newline=~s/^!\/Cray//;}
      if ($sgi=~/TRUE/) {$newline=~s/^!\/SGI//;}
      if ($imp=~/TRUE/) {$newline=~s/^!\/impi//;}
      if ($cvi=~/TRUE/) {$newline=~s/^!CVIS//;}
      if ($mv4=~/TRUE/) {$newline=~s/^!MatL4//;}
      else              {$newline=~s/^!MatL5//;}
      print OUTFILE $newline;
    }
    close file;
    close(OUTFILE);
  }
}
